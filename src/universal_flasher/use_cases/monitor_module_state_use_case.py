import asyncio
from typing import AsyncGenerator

from universal_flasher.entities.module_state import ModuleState
from universal_flasher.entities.gpios import Gpios
from universal_flasher.entities.module import ModuleStatus, RevPiModule
from universal_flasher.entities.leds import LEDManager


async def monitor_module_state_use_case() -> AsyncGenerator[ModuleStatus, None]:
    """
    An asynchronous generator that monitors and yields the module's state changes.

    Continuously checks the state of the module, and if a change is detected, it updates the module's state
    and yields the new state. This allows other parts of the application to react to state changes.

    #### Yields:
        ModuleStatus: The current state of the module whenever it changes.

    #### Example:
        async for state in monitor_module_state_use_case():
            print(f"Module state changed to {state}")
    """
    module = RevPiModule()
    dut_gpio = Gpios()
    led = LEDManager()
    module_state = ModuleState(gpio=dut_gpio, led_manager=led, module=module)
    module_current_state = module.status
    module_previous_state = None
    yield module_current_state
    while True:
        module_current_state = module_state.get_state()
        if module_current_state != module_previous_state:
            module_previous_state = module_current_state
            await module_state.switch_state(module_current_state)

            yield module_current_state

        await asyncio.sleep(1)
