import asyncio
from universal_flasher.entities.module import RevPiModule
from universal_flasher.entities.notification import Notification, NotificationMessage
from universal_flasher.entities.rpiboot import Rpiboot
from universal_flasher.entities.gpios import Gpios, UsbPwr


async def init_module_use_case() -> RevPiModule:
    """
    Asynchronously initializes a module if it is plugged in and not already initialized.
    This involves running the 'rpiboot' utility, resetting the module, and
    powering on USB, followed by refreshing module information and updating its type.

    Returns:
        RevPiModule: The initialized module, or None if the module is not plugged in.
    """
    gpio = Gpios()
    module = RevPiModule()
    rpiboot = Rpiboot()
    notification = Notification()
    if module.is_plugged():
        if not module.is_initialized:
            try:
                async with asyncio.TaskGroup() as tg:
                    tg.create_task(rpiboot.run(modes="msd"))
                    tg.create_task(gpio.reset_module())
                    tg.create_task(gpio.usb_pwr(UsbPwr.ON))
            except asyncio.CancelledError:
                await gpio.usb_pwr(UsbPwr.OFF)
            else:
                module.refresh_module_info()
                module.update_module_type(gpio)
                notification.put_notification(
                    NotificationMessage(
                        type="info",
                        message="The module has been successfully initialized",
                    )
                )
        return module
    else:
        return None
