import asyncio
from universal_flasher.entities.flash import Flasher
from universal_flasher.entities.module import (
    RevPiModule,
    RevPiModuleUnpluggedDuringFlashException,
)
from universal_flasher.entities.notification import Notification, NotificationMessage
from universal_flasher.repositories.revpi_image_repository import RevpiImageRepo


flasher = None
cancel_event = asyncio.Event()
notification = Notification()


def flash_module_use_case(repo: RevpiImageRepo):
    """
    flashes the module with the specified linux image.

    Args:
        repo (RevpiImageRepo): Repository containing linux images.

    Yields:
        int: Progress percentage of the flash operation.

    Raises:
        - GeneratorExit: If the flashing process is canceled.
        - RevPiModuleUnpluggedDuringFlashException: if moudle is unplugged during the flash process.
    """
    global flasher
    global cancel_event
    module = RevPiModule()
    if module.is_plugged():
        image = repo.get_image(module.image_path)
        flasher = Flasher(image, module.msd_path)
        module.is_busy = True
        flash_process = flasher.flash_module(512)
        try:
            for percent in flash_process:
                if flasher.cancel_process or not module.is_busy:
                    flash_process.close()
                    raise GeneratorExit
                else:
                    yield percent
        except BaseException as exc:
            if flasher.cancel_process:
                flasher.cancel_process = False
                cancel_event.set()
                raise
            if not module.is_plugged():
                module.is_error = True
                raise RevPiModuleUnpluggedDuringFlashException from exc
        finally:
            module.is_busy = False


def flash_module_verify_use_case():
    """
    Verifies the integrity of the flashed module.

    Returns:
        bool: True if the verification succeeds, False otherwise.
    """
    global flasher
    global notification

    is_flashed_succesfully = flasher.verify()

    if is_flashed_succesfully:
        notification.put_notification(
            NotificationMessage(
                type="info", message="the module has been successfully flashed"
            )
        )
    else:
        notification.put_notification(
            NotificationMessage(
                type="error", message="the module has not been successfully flashed"
            )
        )

    return is_flashed_succesfully


async def flash_module_cancel_use_case():
    """Cancels an ongoing flash operation and notifies the user."""
    global flasher
    global cancel_event
    global notification
    flasher.cancel_process = True
    await cancel_event.wait()
    notification.put_notification(
        NotificationMessage(type="info", message="flash process has been canceled")
    )
    cancel_event.clear()
