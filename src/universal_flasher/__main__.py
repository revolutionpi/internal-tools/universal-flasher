# if __package__ == "":
#     from os.path import dirname
#     from sys import path

#     # __file__ is package-*.whl/package/__main__.py
#     # Resulting path is the name of the wheel itself
#     package_path = dirname(dirname(__file__))
#     path.insert(0, package_path)


if __name__ == "__main__":
    # from universal_flasher.core.fastapi.app import create_app
    import sys
    import uvicorn

    uvicorn.run(
        "universal_flasher.core.fastapi.app:app",
        host="0.0.0.0",
        reload=True,
    )
