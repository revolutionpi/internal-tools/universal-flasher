from pydantic import BaseModel


class Module(BaseModel):
    type: str
    size: int
    is_initialized: bool
