from fastapi import APIRouter
from fastapi.responses import StreamingResponse

from universal_flasher.interfaces.presenters.notification_presenter import (
    get_notifications,
)


notification_router = APIRouter(prefix="/notifications", tags=["notification"])


@notification_router.get("")
async def notification():
    return StreamingResponse(get_notifications(), media_type="text/event-stream")
