from fastapi import APIRouter

from universal_flasher.repositories.revpi_image_repository import RevpiImageRepo

revpi_image_router = APIRouter(prefix="/images", tags=["revpi_image_repo"])


@revpi_image_router.get("")
def get_images():
    rev_repo = RevpiImageRepo()
    return rev_repo.get_image_list()
