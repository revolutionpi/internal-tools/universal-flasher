from universal_flasher.entities.module import RevPiModule

from fastapi import APIRouter, status
from fastapi.responses import JSONResponse, StreamingResponse
from universal_flasher.interfaces.models.image_model import Image
from universal_flasher.interfaces.models.module_model import Module
from universal_flasher.interfaces.presenters.flash_module_presenter import (
    flash_module_progress,
)
from universal_flasher.use_cases.flash_module_use_cases import (
    flash_module_verify_use_case,
    flash_module_cancel_use_case,
)

from universal_flasher.use_cases.init_module_use_case import init_module_use_case

module_router = APIRouter(prefix="/module", tags=["module"])


@module_router.get("/init")
async def init_module():
    module = await init_module_use_case()
    if module:
        return Module(
            type=module.type,
            size=module.mmc_capacity,
            is_initialized=module.is_initialized,
        )
    return None


@module_router.post("/image")
def select_image(image: Image):
    RevPiModule().image_path = image.image


@module_router.get("/error")
def ack_error():
    RevPiModule().is_error = False


@module_router.get("/flash")
def flash_module():
    return StreamingResponse(flash_module_progress(), media_type="text/event-stream")


@module_router.delete("/flash")
async def flash_module_cancel():
    await flash_module_cancel_use_case()


@module_router.get("/flash/verify", status_code=status.HTTP_200_OK)
def verify_flash_process():
    is_successfully_flashed = flash_module_verify_use_case()
    if not is_successfully_flashed:
        return JSONResponse(
            status_code=404,
            content="The module has not been successfully flashed due to a failure in flashing the image.",
        )
