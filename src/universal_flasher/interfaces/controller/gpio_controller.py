from fastapi import APIRouter
from fastapi.responses import StreamingResponse

from universal_flasher.interfaces.presenters.monitor_module_state_presenter import (
    monitor_module_state_presenter,
)


gpio_router = APIRouter(prefix="/gpios")


@gpio_router.get("/module-plug-event")
async def monitor_module_state():
    return StreamingResponse(
        monitor_module_state_presenter(), media_type="text/event-stream"
    )
