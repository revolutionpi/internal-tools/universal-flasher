import dataclasses
import json
from universal_flasher.entities.notification import Notification, NotificationMessage


async def get_notifications():
    """
    fetches notifications from the queue and yields them as server-sent events.

    This function continuously listens for new notifications and yields each as a formatted SSE message.

    Yields:
        str: A server-sent event formatted string containing the notification data in JSON format.
    """
    notification_queue = Notification()
    while True:
        data: NotificationMessage = await notification_queue.get_notification()
        yield f"data: {json.dumps(dataclasses.asdict(data))}\n\n"
