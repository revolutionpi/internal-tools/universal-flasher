from fastapi import HTTPException
from pydantic import BaseModel
from universal_flasher.entities.module import RevPiModuleUnpluggedDuringFlashException
from universal_flasher.repositories.revpi_image_repository import RevpiImageRepo
from universal_flasher.use_cases.flash_module_use_cases import flash_module_use_case


class FlashProgress(BaseModel):
    percent: int = 0
    is_flashed: bool = False
    is_canceled: bool = False
    is_error: bool = False
    error_message: str = ""


def flash_module_progress():
    revpi_repo = RevpiImageRepo()
    try:
        for percent in flash_module_use_case(revpi_repo):
            flash_progress = FlashProgress(percent=percent)
            yield f"data: {flash_progress.json()}\n\n"
    except GeneratorExit:
        flash_progress = FlashProgress(is_canceled=True)
        yield f"data: {flash_progress.json()}\n\n"

    except RevPiModuleUnpluggedDuringFlashException:
        flash_progress = FlashProgress(
            is_error=True,
            error_message="The module was unplugged during the flash process",
        )
        yield f"data: {flash_progress.json()}\n\n"

    else:
        flash_progress = FlashProgress(percent=100, is_flashed=True)
        yield f"data: {flash_progress.json()}\n\n"
