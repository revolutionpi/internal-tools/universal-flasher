from pydantic import BaseModel
from universal_flasher.use_cases.monitor_module_state_use_case import (
    monitor_module_state_use_case,
)


class ModuleStatePresenter(BaseModel):
    """Presenter class to represent the module state in a serializable format."""

    status: str


async def monitor_module_state_presenter():
    """
    monitors the module state and yields serialized state updates as Server-Sent Events.

    Yields:
        str: A formatted string representing a Server-Sent Event with the module state as JSON.
    """
    async for state in monitor_module_state_use_case():
        yield f"data: {ModuleStatePresenter(status=state.name).json()}\n\n"
