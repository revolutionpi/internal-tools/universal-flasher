from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


from universal_flasher.interfaces.controller.gpio_controller import gpio_router
from universal_flasher.interfaces.controller.module_controller import module_router
from universal_flasher.interfaces.controller.image_repo_controller import (
    revpi_image_router,
)
from universal_flasher.interfaces.controller.notification_controller import (
    notification_router,
)

app = FastAPI(title="Universal Flasher")


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Hello World"}


app.include_router(gpio_router)
app.include_router(module_router)
app.include_router(revpi_image_router)
app.include_router(notification_router)
