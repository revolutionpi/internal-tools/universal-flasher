from dataclasses import dataclass
import os
import hashlib
from typing import List

from universal_flasher.utils.singleton import SingletonMeta


@dataclass
class RevpiImage:
    path: str
    md5hash: str


class RevpiImageRepo(metaclass=SingletonMeta):
    """
    A repository for managing Raspberry Pi images, supporting indexing of images,
    retrieval, and MD5 hash calculation.

    The repository indexes images on initialization, storing their paths and hashes
    for quick access.

    Attributes:
        _images_dict (dict[str, RevpiImage]): A dictionary mapping image names to `RevpiImage` instances.
    """

    BASE_PATH = "/mnt/revpi_image_repo"
    IMAGE_PATH = f"{BASE_PATH}/images"
    HASH_PATH = f"{BASE_PATH}/images_hash"

    def __init__(self) -> None:
        self._images_dict: dict[str, RevpiImage] = {}
        self._index_image()

    def _index_image(self):
        """Indexes images by reading from the IMAGE_PATH and calculating their hashes."""
        for root, _, images in os.walk(self.IMAGE_PATH):
            for image in images:
                path = os.path.join(root, image)
                self._images_dict[image.replace(".img", "")] = RevpiImage(
                    path=path, md5hash=self._image_hash(image, path)
                )

    def _image_hash(self, image_name: str, image_path: str) -> str:
        """
        Calculates or retrieves the MD5 hash of an image.

        Args:
            image_name (str): The name of the image file.
            image_path (str): The full file system path to the image.

        Returns:
            str: The MD5 hash of the image.
        """
        md5_sum_path = os.path.join(self.HASH_PATH, f"{image_name}.md5sum")
        # Try to read the hash from an existing .md5sum file
        try:
            with open(md5_sum_path, "r") as md5_file:
                return md5_file.read().strip()
        except FileNotFoundError:
            pass  # If the file doesn't exist, proceed to calculate the hash

        # Calculate the hash of the image file
        try:
            with open(image_path, "rb") as image:
                hash_md5 = hashlib.md5()
                for chunk in iter(lambda: image.read(4096), b""):
                    hash_md5.update(chunk)
                md5_sum = hash_md5.hexdigest()
        except FileNotFoundError:
            print(f"Image file {image_path} does not exist")

        # Write the new hash to the .md5sum file for future use
        with open(md5_sum_path, "w") as md5_file:
            md5_file.write(md5_sum)

        return md5_sum

    def get_image_list(self) -> List[str]:
        """Returns a list of indexed image names."""
        return list(self._images_dict.keys())

    def get_image(self, image_name: str) -> RevpiImage:
        """
        Retrieves the `RevpiImage` instance for a given image name.

        Args:
            image_name (str): The name of the image to retrieve.

        Returns:
            RevpiImage: The corresponding `RevpiImage` instance.

        Raises:
            KeyError: If the specified image name is not found in the repository.
        """
        try:
            return self._images_dict[image_name]
        except KeyError:
            print("key is not existing")
