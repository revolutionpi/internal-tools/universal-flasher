import asyncio
from enum import Enum
import gpiod

from universal_flasher.utils.singleton import SingletonMeta


# dut GPIO
PLUGGED_1 = 0
PLUGGED_2 = 1
NRPIBOOT = 2
GLOBAL_EN = 3
PLUGGED_CM4 = 8
RESET_MODULE = 9
FEEDBACK_CM4 = 10

# base GPIO
DUT_RUN = 8
DUT_USB_PWR = 9


class UsbPwr(Enum):
    """Enumeration for USB power states."""

    OFF = 0
    ON = 1


class Gpios(metaclass=SingletonMeta):
    """
    This class manages GPIO (General Purpose Input/Output) for a device under test (DUT),
    specifically designed to interact with various pins and perform actions based on their states.

    Attributes:
    -----------
    _dut_chip : gpiod.Chip
        IO-Expander Chip.
    _base_chip: gpiod.Chip
        The GPIO chip for the base system
    _cm_plugged_pins : gpiod.Lines
        GPIO lines to check if CM3 (Compute Module 3) is plugged in.
    _nRPIBOOT_pin : gpiod.Line
        GPIO line for controlling the nRPIBOOT state.
    _global_en : gpiod.Line
        GPIO line for controlling the global enable state.
    _plugged_cm4 : gpiod.Line
        GPIO line to check the plugged state of CM4 (Compute Module 4).
    _reset_module : gpiod.Line
        GPIO line for controlling the reset module state.
    _feedback_cm4 : gpiod.Line
        GPIO line to receive feedback from CM4.
    _dut_run : gpiod.Line
        GPIO line for indicating the DUT's running state.
    _dut_run_pwr : gpiod.Line
        GPIO line for controlling the DUT's USB power.

    """

    def __init__(self) -> None:
        """Initializes GPIO lines and sets their configurations."""
        self._dut_chip = gpiod.Chip("2")
        self._base_chip = gpiod.Chip("0")

        # Dut Chip setup
        self._cm_plugged_pins = self._dut_chip.get_lines([PLUGGED_1, PLUGGED_2])
        self._cm_plugged_pins.request(
            consumer=__class__.__name__,
            type=gpiod.LINE_REQ_DIR_IN,
            flags=gpiod.LINE_REQ_FLAG_ACTIVE_LOW,
        )

        self._nRPIBOOT_pin = self._dut_chip.get_line(NRPIBOOT)
        self._nRPIBOOT_pin.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT, default_val=1
        )

        self._global_en = self._dut_chip.get_line(GLOBAL_EN)
        self._global_en.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT, default_val=1
        )

        self._plugged_cm4 = self._dut_chip.get_line(PLUGGED_CM4)
        self._plugged_cm4.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT, default_val=1
        )

        self._reset_module = self._dut_chip.get_line(RESET_MODULE)
        self._reset_module.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT, default_val=1
        )

        self._feedback_cm4 = self._dut_chip.get_line(FEEDBACK_CM4)
        self._feedback_cm4.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_IN
        )

        # Base Chip
        self._dut_run = self._base_chip.get_line(DUT_RUN)
        self._dut_run.request(consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT)

        self._dut_run_pwr = self._base_chip.get_line(DUT_USB_PWR)
        self._dut_run_pwr.request(
            consumer=__class__.__name__, type=gpiod.LINE_REQ_DIR_OUT
        )

    def __del__(self):
        """Ensures GPIO resources are properly released when the object is deleted."""
        self._dut_chip.close()
        self._base_chip.close()
        print("close")

    @property
    def cm_plugged_pins(self) -> int:
        """
        This function reads the values from {PLUGGED_1, PLUGGED_2} pins and sums them. The sum is used to indicate the plug status.

        Returns:
            int: A sum representing the plug status of the module.

                - 0: Indicates the module is unplugged.
                - 2: Indicates the module is plugged.
        """
        return sum(self._cm_plugged_pins.get_values())

    async def reset_module(self):
        """toggles the reset line to reset the module."""
        self._reset_module.set_value(0)
        await asyncio.sleep(2)  # 2 seconds
        self._reset_module.set_value(1)

    def turn_module_on_off(self, value) -> None:
        self._global_en.set_value(value)

    def is_on(self) -> bool:
        return True if self._global_en.get_value() == 1 else False

    async def usb_pwr(self, usb_pwr: UsbPwr) -> None:
        """
        sets the USB power state based on the provided value.

        Args:
            usb_pwr (UsbPwr): The desired USB power state (ON or OFF).
        """
        self._dut_run.set_value(usb_pwr.value)
        self._dut_run_pwr.set_value(usb_pwr.value)

    def is_cm4(self):
        """
        Checks if a Compute Module 4 is present by reading the feedback line.

        Returns:
            bool: True if CM4 is present, False otherwise.
        """
        return self._feedback_cm4.get_value() == 1
