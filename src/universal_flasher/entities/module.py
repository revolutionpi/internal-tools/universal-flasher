from enum import Enum, auto
import re
import pyudev
from universal_flasher.entities.gpios import Gpios

from universal_flasher.utils.singleton import SingletonMeta


class ModuleStatus(Enum):
    """
    Represents the possible states of a module within the system.

    Each state is associated with specific behaviors and indicators:

    - UNPLUGGED: The module is not connected. No operations can be performed.
    - PLUGGED: The module is connected and ready for normal operations.
    - PLUGGED_BUSY: The module is connected and actively undergoing a flashing operation.

    """

    UNPLUGGED = 0
    PLUGGED = 2
    PLUGGED_BUSY = auto()
    ERROR = auto()


class RevPiModuleException(Exception):
    pass


class RevPiModuleUnpluggedDuringFlashException(RevPiModuleException):
    pass


class RevPiModule(metaclass=SingletonMeta):
    """
    Represents a Raspberry Pi module, encapsulating details such as the module's status,
    Mass Storage Device (MSD) path, type, associated image file for flashing, and its
    Multimedia Card (MMC) capacity.

    This class employs the Singleton design pattern to ensure a single, consistent state
    across the application.

    Attributes:
        _status (ModuleStatus): The current status of the module (e.g., PLUGGED, UNPLUGGED).
        _msd_path (str): The file system path to the module's MSD.
        _type (str): The type of Raspberry Pi module (e.g., CM4, CM3).
        _image_path (str): The path to the image file to be flashed onto the module.
        _mmc_capacity (int): The capacity of the MMC in gigabytes.
        _is_initialized (bool): Flag indicating whether the module's information has been initialized.
        _is_busy (bool): Flag indicating whether the module is currently busy with an operation.
    """

    def __init__(self) -> None:
        self.clean()

    def clean(self):
        """Resets the module's attributes to their default, uninitialized states."""
        self._status: ModuleStatus = ModuleStatus.UNPLUGGED
        self._msd_path: str = ""
        self._type: str = ""
        self._image_path: str = ""
        self._mmc_capacity: int = 0
        self._is_initialized: bool = False
        self._is_busy: bool = False
        self._is_error: bool = False

    @property
    def is_busy(self) -> bool:
        """Indicates whether the module is currently busy with an operation."""
        return self._is_busy

    @is_busy.setter
    def is_busy(self, value: bool) -> None:
        """Sets the busy state of the module."""
        self._is_busy = value

    @property
    def is_error(self) -> bool:
        """Indicates whether the module has an error."""
        return self._is_error

    @is_error.setter
    def is_error(self, value: bool) -> None:
        """Sets the erorr state of the module."""
        self._is_error = value

    @property
    def status(self) -> ModuleStatus:
        """The current status of the module."""
        return self._status

    @status.setter
    def status(self, value: ModuleStatus) -> None:
        """Sets the status of the module."""
        self._status = value

    @property
    def mmc_capacity(self) -> int:
        """The capacity of the module's MMC, in gigabytes."""
        return self._mmc_capacity

    @property
    def is_initialized(self) -> bool:
        """Indicates whether the module's has been fully initialized."""
        return self._is_initialized

    @property
    def msd_path(self) -> str:
        """The file system path to the module's Mass Storage Device."""
        return self._msd_path

    @msd_path.setter
    def msd_path(self, value: str) -> None:
        """Sets the MSD path of the module."""
        self._msd_path = value

    @property
    def type(self) -> str:
        """The type of Raspberry Pi module, such as CM4 or CM3."""
        return self._type

    @type.setter
    def type(self, value: str) -> None:
        """Sets the type of Raspberry Pi module."""
        self._type = value

    @property
    def image_path(self) -> str:
        """The path to the linux image file intended for flashing onto the module."""
        return self._image_path

    @image_path.setter
    def image_path(self, value: str) -> None:
        """Sets the path to the linux image file."""
        self._image_path = value

    def is_plugged(self) -> bool:
        """Checks if the module is currently plugged in."""
        return True if self._status == ModuleStatus.PLUGGED else False

    def update_module_type(self, gpio: Gpios):
        """Determines and updates the type of the Raspberry Pi module based on GPIO status."""
        if gpio.is_cm4():
            self.type = "CM4"
        else:
            self.type = "CM3/CM4S"

    def refresh_module_info(self):
        """
        Refreshes the module's information, such as the MSD path and MMC capacity, by scanning
        connected devices and their attributes.
        """
        dev = pyudev.Context()
        while self._msd_path == "":
            for device in dev.list_devices(ID_VENDOR="RPi-MSD-"):
                if re.search(r"sd[a-z]$", device.device_node):
                    # Calculate the MMC capacity in GB: The 'size' attribute represents the total number of sectors on the device.
                    # (sector count multiplied by the 512-byte block size) into gigabytes by dividing by 1024^3, which is the number of bytes in a gigabyte.
                    size = int(device.attributes.asstring("size")) * 512 / 1024**3
                    self._msd_path = device.device_node
                    self._mmc_capacity = min([8, 16, 32], key=lambda x: abs(x - size))
        self._is_initialized = True
