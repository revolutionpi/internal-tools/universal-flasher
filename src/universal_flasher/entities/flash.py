import hashlib
import math
import os
import time
from typing import NamedTuple, Optional

from universal_flasher.entities.module import RevPiModule
from universal_flasher.repositories.revpi_image_repository import (
    RevpiImage,
    RevpiImageRepo,
)


class ProgressUpdate(NamedTuple):
    percentage: Optional[int]
    next_threshold: int


class Flasher:
    """
    Manages the flashing of a linux image to a specified destination device, offering functionality
    for progress tracking and post-flash integrity verification through MD5 checksum comparison.

    Attributes:
        _revpi_image (RevpiImage): An object representing the linux image to be flashed, containing
                                   the file path and expected MD5 hash for verification.
        _destination (str): The file path to the destination device (e.g., a emmc) where the linux
                            image will be written.
        _cancel_flash_process (bool): A flag to control the cancellation of the ongoing flash process.
        _check_sum (str): The MD5 checksum calculated from the data written to the destination device,
                          used for post-flash verification.
        _transferred_bytes (int): The total number of bytes successfully written to the destination
                                  during the flashing process.

    Constants:
        BLOCK_SIZE (int): The size of each block of data to be written in bytes. Defaults to 4096 bytes (4 KiB).
    """

    BLOCK_SIZE = 1024 * 4

    def __init__(self, revpi_image: RevpiImage, destination: str):
        self._revpi_image = revpi_image
        self._destination: str = destination
        self._cancel_flash_process: bool = False
        self._check_sum: str = ""
        self._transferred_bytes: int = 0

    @property
    def cancel_process(self):
        """
        Gets the current state of the flash process cancellation flag.

        Returns:
            bool: True if the flash process is marked for cancellation, False otherwise.
        """
        return self._cancel_flash_process

    @cancel_process.setter
    def cancel_process(self, value):
        """
        Sets the flash process cancellation flag.

        Args:
            value (bool): The desired state of the cancellation flag.
        """
        self._cancel_flash_process = value

    def __copy_progress(
        self, total_size: int, copied_bytes: int, next_percent_threshold: int
    ) -> ProgressUpdate:
        """
        Calculates the current progress of the flash operation and determines if a progress update
        threshold has been crossed.

        Args:
            total_size (int): The total size of the linux image being flashed.
            copied_bytes (int): The number of bytes that have been written to the destination device so far.
            next_percent_threshold (int): The next progress percentage at which to report an update.

        Returns:
            ProgressUpdate: A named tuple containing the current progress percentage (if an update threshold
                            was crossed) and the next update threshold.
        """

        copied_bytes_percentage = (copied_bytes * 100) / total_size
        if copied_bytes_percentage >= next_percent_threshold:
            next_percent_threshold = math.ceil((copied_bytes / total_size) * 100)
            return math.ceil(copied_bytes_percentage), next_percent_threshold
        return None, next_percent_threshold

    def flash_module(self, block_size: int = BLOCK_SIZE):
        """
        Executes the flashing of the linux image to the destination device, block by block, while
        also computing the MD5 checksum of the written data for later verification.

        This method yields progress updates whenever a percentage threshold is crossed.

        Args:
            block_size (int, optional): The size of each data block to be written, in bytes.
                                        Defaults to Flasher.BLOCK_SIZE.

        Yields:
            int: The current progress percentage whenever a new threshold is crossed.

        Raises:
            Exception: Any exceptions encountered during the flash process are raised for the caller to handle.
        """

        try:
            with open(self._revpi_image.path, "br") as image, open(
                self._destination, "bw+"
            ) as mmc:

                total_image_size = os.stat(image.fileno()).st_size

                next_percent_threshold = 0

                hash_md5 = hashlib.md5()

                for block in iter(lambda: image.read(block_size), b""):

                    mmc.write(block)

                    self._transferred_bytes += len(block)

                    hash_md5.update(block)

                    percentage, next_percent_threshold = self.__copy_progress(
                        total_image_size, image.tell(), next_percent_threshold
                    )

                    if percentage:
                        yield percentage
        except GeneratorExit as e:
            print(f"{e=}")
        else:
            self._check_sum = hash_md5.hexdigest()

    def verify(self) -> bool:
        """
        Verifies the integrity of the flashed linux image by comparing the MD5 checksum of the data
        written to the destination device against the checksum computed during the flashing process.

        Returns:
            bool: True if the checksums match, indicating the flash was successful and the data integrity
                  is maintained; False otherwise.


        """
        md5sum = hashlib.md5()
        with open(self._destination, "br") as msd:
            read_size = 0
            while read_size < self._transferred_bytes:
                to_read = min(self.BLOCK_SIZE, self._transferred_bytes - read_size)
                block = msd.read(to_read)
                md5sum.update(block)
                read_size += len(block)

        tet = f"{self._check_sum=}, {md5sum.hexdigest()=}"
        print(tet)
        return self._check_sum == md5sum.hexdigest()


def benchmark_flash_module(block_sizes: list[int]):
    module = RevPiModule()
    module.refresh_module_info()
    repo = RevpiImageRepo()
    revpi_image = repo.get_image("2023-07-24-revpi-bullseye-arm64-lite")
    flasher = Flasher(revpi_image, module.msd_path)
    for block_size in block_sizes:
        start_time = time.time()
        for _ in flasher.flash_module(block_size):
            pass
        end_time = time.time()
        duration = end_time - start_time
        print(f"Block Size: {block_size} bytes, Time Taken: {duration:.2f} seconds")
        flasher.verify()


if __name__ == "__main__":
    block_sizes = [
        # 256,
        # 512,
        # 1024,
        # 2048,
        # 4096,
        # 8192,
        16384,
        # 32768,
        # 65536,
        # 131072,
    ]
    benchmark_flash_module(block_sizes)
