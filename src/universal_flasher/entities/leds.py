import asyncio
from typing import Literal

from universal_flasher.utils.singleton import SingletonMeta


BASE = "/sys/class/leds/"
Board = Literal["base", "dut"]
Color = Literal["red", "green", "yellow"]


class LEDManager(metaclass=SingletonMeta):
    """
    Provides a unified interface for managing the LED operations (on, off, blink) on specified boards. It implements
    the Singleton design pattern to ensure only one instance handles the LED state, thereby preventing conflicts in
    LED control.

    The class interacts with the Linux filesystem under `/sys/class/leds/` to control the LEDs. Each LED is accessed
    by writing to a 'brightness' file within the corresponding LED's directory, which represents the LED's state.

    Attributes:

        led_files (dict): Stores file handles for the 'brightness' control files of LEDs, with keys formatted as
                          'board:color', mapping to their respective file objects.

    Note:
        The class ensures the cleanup of all open file resources upon deletion of its instance, maintaining resource
        efficiency and preventing file descriptor leaks.
    """

    def __init__(self):
        self.led_files = {}

    def _get_led_file(self, board: Board, color: Color):
        """
        Retrieves or opens the file handle for controlling an LED's brightness based on the specified board and color.

        Args:
            board (Board): The board on which the LED is located ('base' or 'dut').
            color (Color): The color of the LED ('red', 'green', 'yellow').

        Returns:
            file: The file object for the LED's 'brightness' control file.
        """

        # Unique identifier for each LED
        key = f"{board}:{color}"
        # Open the control file for the LED if not already done
        if key not in self.led_files:
            self.led_files[key] = open(
                f"{BASE}{board}:{color}:unknown/brightness", "br+", buffering=0
            )
        return self.led_files[key]

    def led_on(self, board: Board, color: Color):
        """
        Turns the specified LED on by writing '1' to its 'brightness' control file.

        Args:
            board (Board): The board on which the LED is located.
            color (Color): The color of the LED.
        """
        led_file = self._get_led_file(board, color)
        led_file.write(b"1")

    def led_off(self, board: Board, color: Color):
        """
        Turns the specified LED off by writing '0' to its 'brightness' control file.

        Args:
            board (Board): The board on which the LED is located.
            color (Color): The color of the LED.
        """
        led_file = self._get_led_file(board, color)
        led_file.write(b"0")

    async def blink_led(self, board: Board, color: Color, interval: float):
        """
        Makes the specified LED blink with a given interval. This is an asynchronous operation, allowing other tasks
        to run concurrently.

        Args:
            board (Board): The board on which the LED is located.
            color (Color): The color of the LED.
            interval (float): The duration in seconds for each on/off state of the blinking cycle.
        """
        while True:
            self.led_on(board, color)
            await asyncio.sleep(interval)
            self.led_off(board, color)
            await asyncio.sleep(interval)

    def _close(self) -> None:
        """
        Closes all open LED control files and clears the cache, ensuring a clean shutdown and preventing resource leaks.
        """
        for key, file in self.led_files.items():
            file.close()
        self.led_files.clear()

    def __del__(self):
        self._close()
