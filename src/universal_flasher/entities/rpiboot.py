import asyncio
from typing import Literal

from universal_flasher.utils.singleton import SingletonMeta


class Rpiboot(metaclass=SingletonMeta):
    """
    Provides an interface to run the Raspberry Pi USB booting tool 'rpiboot' in different modes.
    Utilizes the Singleton design pattern to ensure that only one instance orchestrates the boot process.

    Attributes:
        _modes (dict): A dictionary mapping the supported 'rpiboot' modes to their respective directory paths
                       where the booting files are located.

    Supported Modes:
        - 'msd': Mass Storage Device mode.
        - 'msg': Mass Storage Gadget mode.

    Note:
        This class assumes the 'rpiboot' utility is installed and accessible in the system's PATH, and that the
        required files for each mode are present in the specified directories.
    """

    def __init__(self) -> None:
        """Initializes the Rpiboot instance with predefined modes and their corresponding directory paths."""
        self._modes = {
            "msd": "/usr/share/rpiboot/msd",
            "msg": "/usr/share/rpiboot/mass-storage-gadget/",
        }

    async def run(self, modes: Literal["msd", "msg"] = "msd"):
        """
        runs the 'rpiboot' utility in the specified mode.

        Args:
            mode (Literal["msd", "msg"], optional): The mode in which to run 'rpiboot'. Defaults to "msd".
        """
        try:

            print("run rpiboot")
            proc = await asyncio.create_subprocess_shell(
                f"rpiboot -d {self._modes[modes]}",
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE,
            )

            stdout, stderr = await proc.communicate()

            print(f"{proc.returncode=} {stdout=} {stderr=}")
        except Exception as e:
            print(f"An exception occurred {e}")
