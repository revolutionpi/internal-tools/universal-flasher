import asyncio
from universal_flasher.entities.gpios import Gpios, UsbPwr
from universal_flasher.entities.leds import LEDManager
from universal_flasher.entities.module import ModuleStatus, RevPiModule
from universal_flasher.entities.notification import Notification, NotificationMessage


class ModuleState:
    """
    Manages the state of a module, reflecting changes through LED indicators and notifications.

    #### Attributes:
        - module (RevPiModule): The module whose state is being managed.
        - gpio (Gpios): GPIO manager to interact with the module's GPIO pins.
        - led_manager (LEDManager): LED manager to control LED indicators based on the module's state.
        - notification (Notification): Notification manager to send status updates.
        - task (Optional[asyncio.Task]): The current asynchronous task for blinking LEDs, reflecting the module's state.
    """

    def __init__(self, module: RevPiModule, gpio: Gpios, led_manager: LEDManager):
        self.led_manager = led_manager
        self.module = module
        self.gpio = gpio
        self.notification = Notification()
        self.task = None

    async def switch_state(self, state: ModuleStatus):
        """
        Switches the module's state to the specified state, updating LEDs and cancelling any ongoing LED tasks.

        #### Args:
            state (ModuleStatus): The new state to transition to.
        """
        state_method = getattr(self, state.name.lower())
        if not self.module.is_error:
            self.led_manager.led_off("dut", "red")
        if self.task:
            self.task.cancel()
        await state_method()

    def get_state(self) -> ModuleStatus:
        """
        Determines the current state of the module based on GPIO inputs and the module's busy status.

        Returns:
            ModuleStatus: The current state of the module.
        """

        status = ModuleStatus.UNPLUGGED
        match self.gpio.cm_plugged_pins:
            case ModuleStatus.UNPLUGGED.value:
                if self.module.is_error:
                    status = ModuleStatus.ERROR
                else:
                    status = ModuleStatus.UNPLUGGED
            case ModuleStatus.PLUGGED.value:
                if self.module.is_busy:
                    status = ModuleStatus.PLUGGED_BUSY
                else:
                    status = ModuleStatus.PLUGGED
        self.module.status = status
        return status

    async def plugged(self):
        self.led_manager.led_on("dut", "green")
        self.led_manager.led_on("dut", "yellow")
        if not self.module.is_initialized:
            self.notification.put_notification(
                NotificationMessage(type="info", message="The module is plugged")
            )

    async def unplugged(self):
        self.led_manager.led_off("dut", "yellow")
        self.notification.put_notification(
            NotificationMessage(type="info", message="The module is unplugged")
        )
        self.module.clean()
        await self.gpio.usb_pwr(UsbPwr.OFF)
        try:
            self.task = asyncio.create_task(
                self.led_manager.blink_led("dut", "green", 1)
            )
        except asyncio.CancelledError:
            ...

    async def plugged_busy(self):
        try:
            self.task = asyncio.create_task(
                self.led_manager.blink_led("dut", "yellow", 1)
            )
        except asyncio.CancelledError:
            ...

    async def error(self):

        self.led_manager.led_on("dut", "red")
