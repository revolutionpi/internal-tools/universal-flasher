import asyncio
from dataclasses import dataclass
from typing import Literal

from universal_flasher.utils.singleton import SingletonMeta


@dataclass
class NotificationMessage:
    """
    Represents a notification message with a specific type and textual content.

    Attributes:
        type (Literal["info", "error", "warning"]): The type of the notification, indicating its nature and potentially how it should be presented.
        message (str): The textual content of the notification message.
    """

    type: Literal["info", "error", "warning"]
    message: str


class Notification(metaclass=SingletonMeta):
    """
    Manages notification messages in an asynchronous manner, allowing various parts of the application
    to enqueue and dequeue notifications as needed.

    This class is implemented as a Singleton to ensure a single, global point of access to the notification
    queue, maintaining consistency across the application.

    Attributes:
        _queue (asyncio.Queue[NotificationMessage]): The asynchronous queue holding notification messages.
    """

    def __init__(self) -> None:
        """Initializes the Notification manager with an empty asyncio queue for notification messages."""
        self._queue: asyncio.Queue[NotificationMessage] = asyncio.Queue()

    def put_notification(self, notification_message: NotificationMessage) -> None:
        """
        Enqueues a notification message into the notification queue.

        Args:
            notification_message (NotificationMessage): The notification message to be enqueued.
        """
        self._queue.put_nowait(notification_message)

    async def get_notification(self) -> NotificationMessage:
        """
        Asynchronously dequeues a notification message from the notification queue.

        Returns:
            NotificationMessage: The dequeued notification message.
        """
        return await self._queue.get()
