# Universal Flasher

## Project Overview

This project aims to develop a Universal Flasher to simplify the process of flashing a Linux image on RevPi devices and modules. The service device will address the complexities and dependencies currently involved in the flashing process.

The Universal Flasher consists of two main software components:
- **Frontend**: Implemented using the JavaScript framework ReactJS, the frontend provides a user-friendly interface for interacting with the Universal Flasher. It allows users to select Linux images, initialize modules, and monitor the flashing process through an integrated touchscreen display.
  
- **Backend**: The backend comprises the core application that implements the primary functionality of the service. It also includes an API, built using the FastAPI framework, which makes the service accessible from the frontend or another systems. The backend handles tasks such as image selection, module initialization, flashing, and verification, ensuring seamless communication between the hardware and software components.

this codebase contains only the backend. The frontend can be found in this [repository.](https://gitlab.com/revolutionpi/internal-tools/universal-flasher-ui) 

## Table of Contents
1. [Requirements](#requirements)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Project Structure](#project-structure)
5. [Project Status](#project-status)


## Requirements
To set up and run the Universal Flasher project, ensure you have the following:

### Hardware
- [Base board](https://kunbus-gmbh.atlassian.net/wiki/spaces/EOL/pages/2930475327/LP0225+Universal+Flasher+Baseboard) 
- [Dut board](https://kunbus-gmbh.atlassian.net/wiki/spaces/EOL/pages/2930802869/LP0227+Universal+Flasher+CM3)
- **Raspberry Pi 7 Display**
### Software
- Python 3.11 or higher
- Linux image must be compiled with the specific [Devicetree](https://gitlab.com/ahmadak53/linux/-/blob/0a13f5680cc496c74e5d841a7c962824b99fa078/arch/arm/boot/dts/overlays/revpi-universal-flasher-overlay.dts) for the Universal Flasher.  
To ensure the correct Devicetree is loaded, add the following entry to the config.txt file:  
`dtoverlay=revpi-universal-flasher`

## Installation
To install and set up the Universal Flasher project, follow these steps:

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/revolutionpi/internal-tools/universal-flasher.git
   ```
2. Navigate to the project directory:
   ```sh
   cd universal-flasher
   ```
3. Install the required dependencies:
   ```sh
   pip install -r requirements.txt
   ```

## Usage
To use the Universal Flasher, follow these steps:

1. Navigate to the `src` directory:
   ```sh
   cd src
   ```
2. Run the application:
   ```sh
   sudo python -m universal_flasher
   ```
3. Once the application is running, you can access the server by navigating to:
   ```sh
   http://localhost:8000/
   ```
4. To check the available endpoints and interact with the API, visit the API documentation at:
   ```sh
   http://localhost:8000/docs
   ```
   This will open an interactive API documentation interface  where you can see all the endpoints, their descriptions, and test them directly from your browser.

### Frontend
After starting the server, you can flash a module using the user interface by following these steps:

1. **Choose Linux Image**:
    - Select the desired Linux image from the dropdown menu.
    - Click "Next" to proceed.
    
    ![Choose Linux Image](docs/images/first_step.png)

2. **Initialize Module**:
   - Plug in the module to be flashed.
   - The system will detect and initialize the module.
    
    ![Initialize Module](docs/images/init.png)
   - Once initialized, the screen will show the module details.
   - Click "Flash" to start the flashing process.
    
    ![Initialize Module](docs/images/info.png)

3. **Flash**: 
   - The flashing process will begin, showing the progress percentage
   - You can cancel the process anytime by clicking "Cancel process."
  
    ![Flash Progress](docs/images/flash_progress.png)

4. **Verifying**: 
   - After flashing, the system will verify the flashed image.
   - Once verification is complete, you will see a success message.
  
    ![Initialize Module](docs/images/succes_flash.png)

## Project Structure
```plaintext
universal-flasher/
└── src
    └── universal_flasher
        ├── core
        │   └── fastapi
        ├── entities
        ├── interfaces
        │   ├── controller
        │   ├── models
        │   └── presenters
        ├── repositories
        └── use_cases
```


### src/universal_flasher Directory
This is the main directory for the Universal Flasher project, containing all core components and modules.

#### core Directory

- **fastapi Directory**: Contains files related to the FastAPI framework, which is used to build the web application for the project.


#### entities Directory

- Contains the business logic entities used throughout the project. These represent the core components of the application.

#### interfaces Directory

- **controller Directory**: Contains controller classes responsible for handling incoming requests, processing them, and returning appropriate responses.
- **models Directory**: Defines the data models used in the application. These models represent the structure of the data and are used for data transfer between different parts of the application.
- **presenters Directory**: Contains presenter classes that format the data for the UI.

#### repositories Directory

- Contains classes and methods for data access and storage.

#### use_cases Directory

- Use cases represent the specific actions or operations that can be performed in the application.


## Project Status

### Completed Features
- [✔️] Backend core application development
- [✔️] API implementation using FastAPI
- [✔️] User interface design using ReactJS
  
### In Progress
- [❗] Addressing issues with the touchscreen to ensure it functions properly.

### Pending Features
- [❗] Creating a custom OS image using DebOS for Universal Flasher device.
- [❗] Implementing an automatic download mechanism for the latest RevPi Linux images
- [❗] Setting up a CI/CD pipeline, including Debian packaging.
- [❗] Developing a server-client architecture to manage  multiple devices (Universal Flasher) simultaneously.
- [❗] Implementing integrated power consumption measurement to identify potential device malfunctions



